#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"
using namespace std;


int main()
{
	BSNode *bsCopy = NULL;

	// Create a binary tree with 10 pairs of numbers
	BSNode* bs = new BSNode("6");
	bs->insert("6");
	bs->insert("2");
	bs->insert("2");
	bs->insert("8");
	bs->insert("8");
	bs->insert("3");
	bs->insert("3");
	bs->insert("5");
	bs->insert("5");

	// Print the nodes in the tree
	cout << "Tree 1:" << endl;
	bs->printNodes();
	cout << endl; // New line
	
	// Try the other functions of the BSNode
	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "Depth of node with 4 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "Depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;
	cout << "Search for 5: " << bs->search("5") << endl;
	cout << "Search for 7: " << bs->search("7") << endl;
	cout << "Search for 8: " << bs->search("8") << endl;

	// New lines
	cout << endl << endl << endl;

	// Copy the tree
	bsCopy = new BSNode(*bs);

	// Print the nodes in the tree
	cout << "Tree 2:" << endl;
	bsCopy->printNodes();
	cout << endl; // New line

	// Try the other functions of the BSNode
	cout << "Tree height: " << bsCopy->getHeight() << endl;
	cout << "Depth of node with 4 depth: " << bsCopy->getLeft()->getRight()->getRight()->getDepth(*bsCopy) << endl;
	cout << "Depth of node with 3 depth: " << bsCopy->getLeft()->getRight()->getDepth(*bsCopy) << endl;
	cout << "Search for 5: " << bsCopy->search("5") << endl;
	cout << "Search for 7: " << bsCopy->search("7") << endl;
	cout << "Search for 8: " << bsCopy->search("8") << endl;

	// Try get depth from node that is in another tree
	cout << "Get depth from node that is in another tree: " << bsCopy->getLeft()->getRight()->getDepth(*bs) << endl;

	string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	delete bs;

	return 0;
}

