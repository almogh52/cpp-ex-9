#pragma once
#include "BSNode.h"

template <class T>
class AVLTree : public BSNode<T>
{
public:
	AVLTree(T value) : BSNode<T>(value)
	{

	}

	virtual AVLTree *insert(T value)
	{
		AVLTree *newThis = this;

		if (value == this->_data)
		{
			throw invalid_argument("Value already exists!");
		}

		if (value < this->_data)
		{
			if (this->_left)
			{
				this->_left = this->_left->insert(value);
			}
			else {
				this->_left = new AVLTree<T>(value);
			}

			if (getBalanceFactor() == 2)
			{
				if (value < this->_left->getData())
				{
					newThis = rotateRight();
				}
				else {
					this->_left = ((AVLTree *)this->_left)->rotateLeft();
					newThis = rotateRight();
				}
			}
		}
		else {
			if (this->_right)
			{
				this->_right = this->_right->insert(value);
			}
			else {
				this->_right = new AVLTree<T>(value);
			}

			if (getBalanceFactor() == -2)
			{
				if (value < this->_right->getData())
				{
					newThis = rotateLeft();
				}
				else {
					this->_right = ((AVLTree *)this->_right)->rotateRight();
					newThis = rotateLeft();
				}
			}
		}

		return newThis;
	}

private:
	int getBalanceFactor() const
	{
		return (this->_left ? this->_left->getHeight() : 0) -
			(this->_right ? this->_right->getHeight() : 0);
	}

	AVLTree *rotateRight()
	{
		AVLTree <T>*leftChildren = (AVLTree <T>*)this->_left;
		this->_left = leftChildren ? leftChildren->_right : NULL;

		if (leftChildren)
		{
			leftChildren->_right = this;
		}

		return leftChildren;
	}

	AVLTree *rotateLeft()
	{
		AVLTree <T>*rightChildren = (AVLTree <T>*)this->_right;
		this->_right = rightChildren ? rightChildren->_left : NULL;

		if (rightChildren)
		{
			rightChildren->_left = this;
		}

		return rightChildren;
	}
};