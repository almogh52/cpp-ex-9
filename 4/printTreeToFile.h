#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include "AVLTree.h"

using namespace std;

template <class T>
void printToFile2(const AVLTree<T> *bs, ofstream& outputFile)
{
	outputFile << bs->getData() << " ";

	if (bs->getLeft())
	{
		printToFile2((AVLTree<T> *)bs->getLeft(), outputFile);
	}
	else {
		outputFile << "# ";
	}

	if (bs->getRight())
	{
		printToFile2((AVLTree<T> *)bs->getRight(), outputFile);
	}
	else {
		outputFile << "# ";
	}
}


template <class T>
void printTreeToFile(const AVLTree<T>* bs, std::string output)
{
	ofstream fileHandle(output);

	printToFile2(bs, fileHandle);
}