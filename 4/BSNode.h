#ifndef BSNode_H
#define BSNode_H

#include <iostream>
#include <string>
#include <algorithm>

#define NOT_FOUND -1

using namespace std;

template <class T>
class BSNode
{
public:
	/*
	* This is the main constructor of the class BSNode
	* param data: The data of the node
	*/
	BSNode(T data) : _data(data), _right(NULL), _left(NULL), _count(1)
	{

	}

	/*
	* This is the copy constructor of the class BSNode
	* param other: The other BSNode to copy from
	*/
	BSNode(const BSNode& other)
	{
		// Using the = operator deep copy the tree
		*this = other;
	}

	/*
	* This is the destructor of the class BSNode
	*/
	virtual ~BSNode()
	{
		// If the node has left node free it
		if (_left)
		{
			delete _left;
		}

		// If the node has right node free it
		if (_right)
		{
			delete _right;
		}
	}
	
	/*
	* This function inserts a new value to the tree
	* param value: The new value to be inserted into the tree
	* return: The pointer to the root node
	*/
	virtual BSNode *insert(T value)
	{
		// If the value equals to the current node's data, increase the count field of this node
		if (value == _data)
		{
			_count++;
			return this;
		}

		// If the new value is smaller than the current value, try insert it to the left node tree
		if (value < _data)
		{
			// If the left children is clear, create the left children with the value
			if (!_left)
			{
				_left = new BSNode(value);
			}
			else {
				// Insert the value to the left tree
				_left->insert(value);
			}
		}
		else {
			// If the right children is clear, create the right children with the value
			if (!_right)
			{
				_right = new BSNode(value);
			}
			else {
				// Insert the value to the right tree
				_right->insert(value);
			}
		}

		return this;
	}

	/*
	* This is = operator for the BSNode class, it's used as copy function
	* param other: The other BSNode to copy from
	* return: The reference to this object
	*/
	BSNode& operator=(const BSNode& other)
	{
		// If the other BSNode has a left node, copy it
		if (other._left)
		{
			// Deep copy of the left node tree
			_left = new BSNode(*other._left);
		}
		else {
			_left = NULL; // Default value
		}

		// If the other BSNode has a right node, copy it
		if (other._right)
		{
			// Deep copy of the left node tree
			_right = new BSNode(*other._right);
		}
		else {
			_right = NULL; // Default value
		}

		// Copy data and count fields
		_data = other._data;
		_count = other._count;

		return *this;
	}

	/*
	* This function checks if the current node is a leaf in the tree
	* return: True if it's a leaf and false otherwise
	*/
	bool isLeaf() const
	{
		// The node is a leaf if it doesn't have any sons
		return _left == NULL && _right == NULL;
	}

	T getData() const
	{
		return _data;
	}

	BSNode* getLeft() const
	{
		return _left;
	}

	BSNode* getRight() const
	{
		return _right;
	}

	/*
	* This function searches for a value in the tree
	* param val: The value to be searched
	* return: True if the value was found and false otherwise
	*/
	bool search(T val) const
	{
		// If the current node's data is the searched val return true
		if (_data == val)
		{
			return true;
		}
		else if (isLeaf()) // If the current node is a leaf and it's data isn't the searched val return false
		{
			return false;
		}

		// Search for the value in the children (Search only if children isn't NULL)
		return ((_left && _left->search(val)) ||
			(_right && _right->search(val)));
	}

	/*
	* This functions gets the tree's height from the current node
	* return: The height of the tree
	*/
	int getHeight() const
	{
		// If the curret node is a leaf in the tree it means the tree has height of 1
		if (isLeaf())
		{
			return 1;
		}

		// Get the highest height returns from the children of this node (only get height if the children isn't null)
		return 1 + std::max(_left ? _left->getHeight() : 0,
			_right ? _right->getHeight() : 0);
	}

	/*
	* This function gets this node's depth in relation to the root node
	* param root: The reference to the root node
	* return: The depth of this node if found on the root's tree or NOT_FOUND otherwise
	*/
	int getDepth(const BSNode& root) const
	{
		// Get the dist of this node from the root node
		return root.getCurrNodeDistFromInputNode(this);
	}

	/*
	* This function prints all the nodes that are in the tree
	* return: None
	*/
	void printNodes() const
	{
		// If there is a left children, call it to print it's nodes first because it should be smaller
		if (_left)
		{
			_left->printNodes();
		}

		// Print the node's data and appearance counts
		std::cout << _data << " " << _count << std::endl;

		// If there is a right children, call it to print it's nodes last because it should be bigger
		if (_right)
		{
			_right->printNodes();
		}
	}

protected:
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B

	/*
	* This function gets the distance of a node from this node
	* param node: The pointer to the node
	* return: The distance of the node if found or NOT_FOUND otherwise
	*/
	int getCurrNodeDistFromInputNode(const BSNode* node) const
	{
		int rightDist = NOT_FOUND, leftDist = NOT_FOUND;

		// If the node is this node, return 1 distance between the nodes
		if (this == node)
		{
			return 1;
		}
		else if (isLeaf()) // If the current node is a leaf and the node isn't found return NOT_FOUND
		{
			return NOT_FOUND;
		}

		// If the node has a left node, get the dist for the node from there
		if (_left)
		{
			leftDist = _left->getCurrNodeDistFromInputNode(node);
		}

		// If the node has a right node and the dist wasn't found from the left node, get the dist for the node from there
		if (_right && leftDist == NOT_FOUND)
		{
			rightDist = _right->getCurrNodeDistFromInputNode(node);
		}

		// If both dists aren't found, return NOT_FOUND, else get the bigger value of the two dists + the current node
		return (leftDist == NOT_FOUND && rightDist == NOT_FOUND ?
			NOT_FOUND :
			std::max(1 + leftDist, 1 + rightDist));
	}

};

#endif