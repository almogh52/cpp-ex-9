#include "AVLTree.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"
using namespace std;


int main()
{

	AVLTree<int> *tree = new AVLTree<int>(5);
	tree = tree->insert(2);
	tree = tree->insert(7);
	tree = tree->insert(1);
	tree = tree->insert(4);
	tree = tree->insert(9);
	tree = tree->insert(16);
	tree = tree->insert(15);
	tree = tree->insert(3);

	string textTree = "BSTData.txt";
	printTreeToFile(tree, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	delete tree;

	return 0;
}

