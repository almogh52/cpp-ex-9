#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>

#define ARR_SIZE 15

using namespace std;

int main()
{
	BSNode <string> *stringTree = NULL;
	BSNode <int> *intTree = NULL;

	string strArr[ARR_SIZE] = { "b", "a", "d", "z", "y", "q", "w", "c", "j", "i", "r", "x", "v", "m", "n" };
	int intArr[ARR_SIZE] = { 52, 17, 19, 10, 85, 84, 86, 23, 77, 11, 1, 0, -5, -512, 8741 };

	// Print the int array
	for (int i = 0; i < ARR_SIZE; i++)
	{
		std::cout << intArr[i] << " ";
	}

	std::cout << std::endl; // New line

	// Print the string array
	for (int i = 0; i < ARR_SIZE; i++)
	{
		std::cout << strArr[i] << " ";
	}

	std::cout << std::endl; // New line

	// Insert the arrays into the trees
	for (int i = 0; i < ARR_SIZE; i++)
	{
		// Check if the head is NULL
		if (!intTree)
		{
			// Initialize the int tree using the first value
			intTree = new BSNode<int>(intArr[i]);
		}
		else {
			// Normal insert
			intTree->insert(intArr[i]);
		}
		
		// Check if the head is NULL
		if (!stringTree)
		{
			// Initialize the int tree using the first value
			stringTree = new BSNode<string>(strArr[i]);
		}
		else {
			// Normal insert
			stringTree->insert(strArr[i]);
		}

	}

	// Print int tree
	std::cout << "Int tree:" << std::endl;
	intTree->printNodes();
	std::cout << std::endl;

	// Print str tree
	std::cout << "Str tree:" << std::endl;
	stringTree->printNodes();
	std::cout << std::endl;

	system("pause");

	// Free memory
	delete intTree, stringTree;

	return 0;
}

