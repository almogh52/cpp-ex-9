#include "functions.h"
#include "Sample.h"
#include <iostream>

int main() {
	const int arr_size = 5;

	std::cout << "Double checks:" << std::endl;
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;
	
	std::cout << "Char checks:" << std::endl;
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>('c', 'f') << std::endl;
	std::cout << compare<char>('z', 'e') << std::endl;
	std::cout << compare<char>('a', 'a') << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	char charArr[arr_size] = { 'z', 'a', 'c', 'b', 'p' };
	bubbleSort<char>(charArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, arr_size);
	std::cout << std::endl;

	std::cout << "Sample checks:" << std::endl;
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<Sample>(Sample(2), Sample(4)) << std::endl;
	std::cout << compare<Sample>(Sample(7), Sample(4)) << std::endl;
	std::cout << compare<Sample>(Sample(3), Sample(3)) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	Sample sampleArr[arr_size] = { Sample(52), Sample(1), Sample(63), Sample(10), Sample(0) };
	bubbleSort<Sample>(sampleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << sampleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<Sample>(sampleArr, arr_size);
	std::cout << std::endl;

	system("pause");
	return 0;
}