#pragma once
#include <iostream>

#define BIGGER -1
#define EQUAL 0
#define SMALLER 1

/*
* This functions compares two objects from type T.
* param a: The first element to be checked.
* param b: The second element to be checked
* return: -1 if a is bigger, 1 if a is smaller and 0 if they are equal
*/
template <class T>
int compare(T a, T b)
{
	// Check if the first element is bigger
	if (a < b)
	{
		return SMALLER;
	}
	else if (a > b) // Check if the first element is smaller
	{
		return BIGGER;
	}

	return EQUAL; // Check if the elements are equal
}

/*
* This functions sorts an array from type T using bubble sort
* param arr: The pointer to the arrray
* param len: The length of the array
* return: None
*/
template <class T>
void bubbleSort(T *arr, int len)
{
	T temp;

	// Go through the elements and move each one to it's correct position in the array
	for (int i = 0; i < len - 1; i++)
	{
		// Last i elements are already in place so check all the elements that aren't sorted yet
		for (int j = 0; j < len - i - 1; j++)
		{
			// If the element is bigger than the next element swap
			if (compare<T>(arr[j], arr[j+1]) == BIGGER)
			{
				// Swap the elements
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

/*
* This functions prints an array from type T
* param arr: The pointer to the arrray
* param len: The length of the array
* return: None
*/
template <class T>
void printArray(T *arr, int len)
{
	// Go through the elements of the array and print each one
	for (int i = 0; i < len; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}