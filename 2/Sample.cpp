#include "Sample.h"

/*
* This is the default constructor of the class Sample
*/
Sample::Sample()
{
}

/*
* This is the constructor of the class Sample
* param data: The data of the Sample class
*/
Sample::Sample(int data) : _data(data)
{
}

/*
* This functions implements the operator <
* param other: The other Sample object to comapre with
* return: True if the other is bigger or False otherwise
*/
bool Sample::operator<(const Sample & other) const
{
	// Check if the other data is bigger than this object's data
	return _data < other._data;
}

/*
* This functions implements the operator >
* param other: The other Sample object to comapre with
* return: True if the other is smaller or False otherwise
*/
bool Sample::operator>(const Sample & other) const
{
	// Check if the other data is smaller than this object's data
	return _data > other._data;
}

/*
* This functions implements the operator ==
* param other: The other Sample object to comapre with
* return: True if the other is equal to this object or False otherwise
*/
bool Sample::operator==(const Sample & other) const
{
	// Check if the other data is equals to this object's data
	return _data == other._data;
}

/*
* This functions implements the operator <<
* param os: The stream to print to
* param other: The other Sample object to comapre with
* return: The stream that was printed to
*/
std::ostream & operator<<(std::ostream & os, const Sample & other)
{
	// Print data to the stream
	return os << other._data;
}
