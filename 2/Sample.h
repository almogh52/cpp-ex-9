#pragma once
#include <iostream>

class Sample
{
public:
	Sample();
	Sample(int data);

	bool operator<(const Sample& other) const;
	bool operator>(const Sample& other) const;
	bool operator==(const Sample& other) const;

private:
	int _data;

	friend std::ostream & operator<<(std::ostream &os, const Sample& other);
};

